package ru.arubtsova.tm.service;

import ru.arubtsova.tm.api.repository.IProjectRepository;
import ru.arubtsova.tm.api.service.IProjectService;
import ru.arubtsova.tm.exception.empty.EmptyNameException;
import ru.arubtsova.tm.exception.empty.EmptyUserIdException;
import ru.arubtsova.tm.model.Project;

public class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
        return project;
    }

}
