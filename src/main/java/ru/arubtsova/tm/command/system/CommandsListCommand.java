package ru.arubtsova.tm.command.system;

import ru.arubtsova.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandsListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String description() {
        return "show application commands.";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        System.out.println("Available commands:");
        for (final AbstractCommand command : commands) {
            final String name = command.name();
            if (name == null) continue;
            System.out.println(name);
        }
    }

}