package ru.arubtsova.tm.command.project;

import ru.arubtsova.tm.command.AbstractProjectCommand;
import ru.arubtsova.tm.exception.entity.ProjectNotFoundException;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Override
    public String description() {
        return "update a project by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Project:");
        System.out.println("Enter Project Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter New Project Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter New Project Description:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = serviceLocator.getProjectService().updateByIndex(userId, index, name, description);
        if (projectUpdate == null) throw new ProjectNotFoundException();
        System.out.println("Project was successfully updated");
    }

}
