package ru.arubtsova.tm.command.project;

import ru.arubtsova.tm.command.AbstractProjectCommand;
import ru.arubtsova.tm.exception.entity.ProjectNotFoundException;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.util.TerminalUtil;

public class ProjectFinishByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-finish-by-name";
    }

    @Override
    public String description() {
        return "change project status to Complete by project name.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Project:");
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().finishByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
    }

}
