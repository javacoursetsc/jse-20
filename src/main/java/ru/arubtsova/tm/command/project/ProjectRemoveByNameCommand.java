package ru.arubtsova.tm.command.project;

import ru.arubtsova.tm.command.AbstractProjectCommand;
import ru.arubtsova.tm.exception.entity.ProjectNotFoundException;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.util.TerminalUtil;

public class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    public String description() {
        return "delete a project by name.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Project Removal:");
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Project was successfully removed");
    }

}
