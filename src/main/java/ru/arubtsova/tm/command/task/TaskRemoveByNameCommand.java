package ru.arubtsova.tm.command.task;

import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.model.Task;
import ru.arubtsova.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {

    public String arg() {
        return null;
    }

    public String name() {
        return "task-remove-by-name";
    }

    public String description() {
        return "delete a task by name.";
    }

    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Task Removal:");
        System.out.println("Enter Task Name:");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().removeByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Task was successfully removed");
    }

}
