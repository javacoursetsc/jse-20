package ru.arubtsova.tm.command.task;

import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.model.Task;
import ru.arubtsova.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-create";
    }

    @Override
    public String description() {
        return "create new task.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Task Create:");
        System.out.println("Enter Task Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Task Description:");
        final String description = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().add(userId, name, description);
        if (task == null) throw new TaskNotFoundException();
    }

}
