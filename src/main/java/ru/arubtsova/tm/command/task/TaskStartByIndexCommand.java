package ru.arubtsova.tm.command.task;

import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.model.Task;
import ru.arubtsova.tm.util.TerminalUtil;

public class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-start-by-index";
    }

    @Override
    public String description() {
        return "change task status to In progress by task index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Task:");
        System.out.println("Enter Task Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().startByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
    }

}
