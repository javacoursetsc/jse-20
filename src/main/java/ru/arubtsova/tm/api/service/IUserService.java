package ru.arubtsova.tm.api.service;

import ru.arubtsova.tm.api.IService;
import ru.arubtsova.tm.enumerated.Role;
import ru.arubtsova.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User findByLogin(String login);

    User findByEmail(String email);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    User updateUser(String userId, String firstName, String lastName, String middleName);

    User removeByLogin(String login);

}
