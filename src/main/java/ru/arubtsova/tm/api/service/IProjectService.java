package ru.arubtsova.tm.api.service;

import ru.arubtsova.tm.api.IBusinessService;
import ru.arubtsova.tm.model.Project;

public interface IProjectService extends IBusinessService<Project> {

    Project add(String userId, String name, String description);

}
