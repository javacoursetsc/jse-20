package ru.arubtsova.tm.api.repository;

import ru.arubtsova.tm.api.IRepository;
import ru.arubtsova.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

}
